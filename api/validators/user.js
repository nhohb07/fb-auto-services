const _ = require('lodash');

async function login(req, res, next) {
  try {
    const { email, name, id, accessToken } = req.body;

    if (_.some([email, name, id, accessToken], (item) => _.isNil(item))) {
      throw new Error();
    }

    next();
  } catch (error) {
    res.status(400).json();
  }
};

async function updateProfile(req, res, next) {
  try {
    const { providerFullAccessToken, id } = req.body;

    if (_.some([providerFullAccessToken, id], (item) => _.isNil(item))) {
      throw new Error();
    }

    next();
  } catch (error) {
    res.status(400).json();
  }
};

module.exports = {
  login,
  updateProfile,
};
