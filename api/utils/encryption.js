const crypto = require('crypto');
const config = require('config');

class Encryption {
  encrypt(string) {
    const cipher = crypto.createCipher('aes-256-cbc', config.app.jwtKey);
    let crypted = cipher.update(string, 'utf-8', 'hex');
    crypted += cipher.final('hex');

    return crypted;
  }

  decrypt(string) {
    const decipher = crypto.createDecipher('aes-256-cbc', config.app.jwtKey);
    let decrypted = decipher.update(string, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');

    return decrypted;
  }
}

module.exports = new Encryption();
