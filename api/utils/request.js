const httpRequest = require('request');
const request = httpRequest.defaults({
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
});

class Request {
  request(method, endpoint, data = {}, options = {}) {
    return new Promise((resolve, reject) => {
      const requestData = {
        method,
        uri: endpoint,
      };

      if (options.headers) {
        requestData.headers = options.headers;
      }

      request(requestData, (error, response, body) => {
        if (typeof body === 'string') {
          body = JSON.parse(body);
        }

        if (error || body.error) {
          return reject(error || body.error);
        }

        resolve(body);
      });
    });
  }

  async get(endpoint, data = {}, options = {}) {
    return await this.request('GET', endpoint, data, options);
  }

  async post(endpoint, data = {}, options = {}) {
    return await this.request('POST', endpoint, data, options);
  }

  async put(endpoint, data = {}, options = {}) {
    return await this.request('PUT', endpoint, data, options);
  }

  async delete(endpoint, data = {}, options = {}) {
    return await this.request('DELETE', endpoint, data, options);
  }

  async head(endpoint, data = {}, options = {}) {
    return await this.request('HEAD', endpoint, data, options);
  }
}

module.exports = new Request();
