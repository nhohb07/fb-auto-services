const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];

    req.userData = jwt.verify(token, config.app.jwtKey);

    next();
  } catch (error) {
    res.status(401).json({
      message: 'You are not authenticated!',
    });
  }
};
