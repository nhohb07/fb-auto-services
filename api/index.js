const path = require('path');
const config = require('config');

// Connect MongoDB
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.connect(config.app.dbURL, { useNewUrlParser: true });

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('port', config.get('app.port'));

// Routes
const userRoutes = require('./routes/user');
app.use('/api/user', userRoutes);
const fbGroupRoutes = require('./routes/facebook-groups');
app.use('/api/fb-groups', fbGroupRoutes);
const fbCommentRoutes = require('./routes/facebook-comments');
app.use('/api/fb-comments', fbCommentRoutes);
const contentRoutes = require('./routes/contents');
app.use('/api/contents', contentRoutes);
const commentRoutes = require('./routes/comments');
app.use('/api/comments', commentRoutes);

// UI
app.use(express.static(path.join(__dirname, '/../client/build')));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  console.log('error handler', err.message);
  res.json({
    success: false,
    error: err.message,
  });
});

const server = app.listen(app.get('port'), () => {
  console.log(`Environment => ${config.util.getEnv('NODE_ENV')}`);
});

module.exports = app;
