const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const contentSchema = mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  content: {
    type: String,
    required: true,
  },
  meta_data: {
    type: {},
  },
}, { timestamps: true });

contentSchema.plugin(uniqueValidator);

module.exports = {
  ContentSchema: contentSchema,
  Content: mongoose.model('Content', contentSchema),
};
