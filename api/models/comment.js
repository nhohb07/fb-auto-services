const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const commentSchema = mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  contents: {
    type: [{}],
    required: true,
  },
  random_content: {
    type: Boolean,
    required: true,
  },
  fb_group_id: {
    type: String,
    required: true,
    unique: true,
  },
  fb_group_name: {
    type: String,
    required: true,
  },
  interval: {
    type: Number,
    required: true,
  },
  next_time: {
    type: Date,
    required: true,
  },
}, { timestamps: true });

commentSchema.plugin(uniqueValidator);

module.exports = {
  CommentSchema: commentSchema,
  Comment: mongoose.model('Comment', commentSchema),
};
