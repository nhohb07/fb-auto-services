const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  provider_id: {
    type: String,
    required: true,
    unique: true,
  },
  provider_name: {
    type: String,
    required: true,
  },
  provider_access_token: {
    type: String,
    required: true,
  },
  provider_full_access_token: {
    type: String,
  },
  provider_meta_data: {
    type: mongoose.Schema.Types.Mixed,
  },
}, { timestamps: true });

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
