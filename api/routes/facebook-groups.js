const express = require('express');
const checkAuth = require('../middleware/check-auth');

const fbGroupCtrl = require('../controllers/facebook-groups');

const router = express.Router();

router.get('', checkAuth, fbGroupCtrl.getAll);

module.exports = router;
