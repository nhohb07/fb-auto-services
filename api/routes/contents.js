const express = require('express');
const checkAuth = require('../middleware/check-auth');

const contentsCtrl = require('../controllers/contents');

const router = express.Router();

router.post('', checkAuth, contentsCtrl.create);
router.get('', checkAuth, contentsCtrl.get);
router.put('', checkAuth, contentsCtrl.update);
router.delete('', checkAuth, contentsCtrl.remove);

module.exports = router;
