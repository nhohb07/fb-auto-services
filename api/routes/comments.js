const express = require('express');
const checkAuth = require('../middleware/check-auth');

const commentsCtrl = require('../controllers/comments');

const router = express.Router();

router.post('', checkAuth, commentsCtrl.create);
router.get('', checkAuth, commentsCtrl.get);
router.put('', checkAuth, commentsCtrl.update);
router.delete('', checkAuth, commentsCtrl.remove);

module.exports = router;
