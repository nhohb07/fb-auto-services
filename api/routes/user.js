const express = require('express');
const checkAuth = require('../middleware/check-auth');
const userValidator = require('../validators/user');

const userCtrl = require('../controllers/user');

const router = express.Router();

router.post('', userValidator.login, userCtrl.login);
router.get('', checkAuth, userCtrl.getProfile);
router.put('', checkAuth, userValidator.updateProfile, userCtrl.updateProfile);

module.exports = router;
