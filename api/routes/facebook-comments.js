const express = require('express');
const checkAuth = require('../middleware/check-auth');

const fbCommentCtrl = require('../controllers/facebook-comments');

const router = express.Router();

router.post('', checkAuth, fbCommentCtrl.create);

module.exports = router;
