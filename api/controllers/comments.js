const addMinutes = require('date-fns/add_minutes');

const { Comment } = require('../models/comment');
const { ObjectId } = require('mongodb');

const pick = require('lodash/pick');

async function create(req, res, next) {
  let commentItem = {};

  try {
    const { contents, groups, interval, random_content } = req.body;

    const comments = [];

    for (const group of groups) {
      commentItem = {
        user_id: ObjectId(req.userData._id),
        contents: contents.map((item) => (pick(item, ['_id', 'name', 'content', 'meta_data']))),
        random_content,
        fb_group_id: group.id,
        fb_group_name: group.name,
        interval: Number(interval),
        next_time: addMinutes(new Date(), Number(interval)),
      };

      const commentCreateResult = await Comment.create(commentItem);

      comments.push(commentCreateResult);
    }

    res.json({
      data: comments,
      success: true,
    });
  } catch (e) {
    let message = e.message;
    if (/expected `fb_group_id` to be unique/gi.test(message)) {
      message = `Group '${commentItem.fb_group_name}' is already exists, please use Edit feature if you want to modify it.`;
    }

    res.json({
      success: false,
      error: message,
    });
  }
}

async function get(req, res, next) {
  try {
    const items = await Comment.find({
      user_id: ObjectId(req.userData._id),
    });

    res.json({
      items,
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

async function update(req, res, next) {
  try {
    const { name, content, _id } = req.body;

    const query = { _id };
    const update = {
      name,
      content,
    };

    const data = await Comment.findOneAndUpdate(query, update, { new: true });

    res.json({
      data,
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

async function remove(req, res, next) {
  try {
    await Comment.deleteOne({ _id: req.body.id });

    res.json({
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}


module.exports = {
  create,
  get,
  update,
  remove,
};
