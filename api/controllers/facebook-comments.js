async function create(req, res, next) {
  try {
    res.json({
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

module.exports = {
  create,
};
