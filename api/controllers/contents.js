const { Content } = require('../models/content');
const { ObjectId } = require('mongodb');

async function create(req, res, next) {
  try {
    const data = await Content.create({
      user_id: ObjectId(req.userData._id),
      name: req.body.name,
      content: req.body.content,
    });

    res.json({
      data,
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

async function get(req, res, next) {
  try {
    const items = await Content.find({
      user_id: ObjectId(req.userData._id),
    });

    res.json({
      items,
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

async function update(req, res, next) {
  try {
    const { name, content, _id } = req.body;

    const query = { _id };
    const update = {
      name,
      content,
    };

    const data = await Content.findOneAndUpdate(query, update, { new: true });

    res.json({
      data,
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

async function remove(req, res, next) {
  try {
    await Content.deleteOne({ _id: req.body.id });

    res.json({
      success: true,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}


module.exports = {
  create,
  get,
  update,
  remove,
};
