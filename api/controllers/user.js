const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');

const Encryption = require('../utils/encryption');
const User = require('../models/user');
const Facebook = require('./facebook');
/**
 * Query User from DB
 * @param {String} provider_id
 * @return {User} User
 */
async function getUser(provider_id) {
  const user = await User.findOne({ provider_id }).exec();

  return user;
}

/**
 * Create User
 * @param {Object} data
 * @return {User} User on success or null on error
 */
async function createOrUpdateUser(data) {
  const query = { provider_id: data.id };
  const user = {
    email: data.email,
    name: data.name,
    provider_id: data.id,
    provider_name: 'facebook',
    provider_access_token: Encryption.encrypt(data.accessToken),
    provider_meta_data: {
      picture: data.picture.data,
    },
  };

  return await User.findOneAndUpdate(query, user, { upsert: true, new: true });
}

/**
 * User login
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {void}
 */
async function login(req, res, next) {
  try {
    const isValidFBToken = await Facebook.verifyAccessToken(req.body.accessToken);
    if (!isValidFBToken) {
      return res.status(400).json({
        message: 'Provider access token is invalid!',
      });
    }

    let user = await createOrUpdateUser(req.body);

    const userToken = jwt.sign({
      _id: user._id,
      provider_id: user.provider_id,
    }, config.app.jwtKey, { expiresIn: '1h' });

    res.json({
      success: true,
      data: {
        userToken,
        email: user.email,
        name: user.name,
        provider_id: user.provider_id,
        provider_full_access_token: !_.isEmpty(user.provider_full_access_token),
      },
    });
  } catch (error) {
    res.json({
      success: false,
      error,
    });
  }
};

/**
 * Get User Profile
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {void}
 */
async function getProfile(req, res, next) {
  let user = await getUser(req.userData.provider_id);

  if (!user) {
    return res.json({
      success: false,
      data: {},
    });
  }

  res.json({
    success: true,
    data: {
      email: user.email,
      name: user.name,
      provider_id: user.provider_id,
      provider_full_access_token: !_.isEmpty(user.provider_full_access_token),
    },
  });
}

/**
 * Update User Profile
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {void}
 */
async function updateProfile(req, res, next) {
  const query = { provider_id: req.body.id };
  const update = req.body;

  if (update.providerFullAccessToken) {
    // Validate access token
    const isValidFBToken = await Facebook.verifyAccessToken(update.providerFullAccessToken);
    if (!isValidFBToken) {
      return res.status(400).json({
        message: 'Provider access token is invalid!',
      });
    }

    update.provider_full_access_token = Encryption.encrypt(update.providerFullAccessToken);
  }

  await User.findOneAndUpdate(query, update, { upsert: true });

  res.json({
    success: true,
  });
}

module.exports = {
  getUser,
  login,
  getProfile,
  updateProfile,
};
