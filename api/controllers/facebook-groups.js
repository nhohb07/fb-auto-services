const Facebook = require('./facebook');

async function getAll(req, res, next) {
  try {
    const groups = await Facebook.graphAPI(req.userData.provider_id, 'me/groups');

    res.json({
      success: true,
      items: groups.data,
    });
  } catch (e) {
    res.json({
      success: false,
      error: e.message,
    });
  }
}

module.exports = {
  getAll,
};
