const Request = require('../utils/request');
const Encryption = require('../utils/encryption');
const User = require('../models/user');

async function getUserAccessToken(provider_id) {
  const user = await User.findOne({ provider_id }).exec();
  return Encryption.decrypt(user.provider_full_access_token);
}

async function verifyAccessToken(token) {
  try {
    await Request.get(`https://graph.facebook.com/me?access_token=${token}`);
    return true;
  } catch (e) {
    return false;
  }
}

async function graphAPI(provider_id, action) {
  const accessToken = await getUserAccessToken(provider_id);
  return await Request.get(`https://graph.facebook.com/${action}?access_token=${accessToken}`);
}

module.exports = {
  verifyAccessToken,
  graphAPI,
};
