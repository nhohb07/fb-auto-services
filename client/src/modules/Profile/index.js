/* eslint-disable camelcase */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions, userActions } from 'reducer/actions';

import { Layout } from 'modules';
import { Alert } from 'components';

import User from 'services/User';

export class _Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      error: null,
    };
  }

  handleChange = (event) => {
    this.setState({
      value: event.target.value,
      error: null,
    });
  }

  updateUser = async (event) => {
    event.preventDefault();
    const { value } = this.state;

    if (!value) {
      return this.setState({
        error: true,
        errorMessage: 'Please enter your access token!',
      });
    }

    if (value === this.props.user.provider_full_access_token) {
      return this.setState({
        error: true,
        errorMessage: 'No change for update!',
      });
    }

    const { spinner } = this.props.actions;
    spinner.show();

    const updateResult = await User.update({ providerFullAccessToken: value });

    spinner.hide();

    this.setState({
      error: (!updateResult.success),
      errorMessage: updateResult.error,
    });
  }

  render() {
    const {
      name = '',
      email = '',
      provider_full_access_token = '',
    } = this.props.user;

    const { value, error, errorMessage } = this.state;

    return (
      <Layout>
        <form onSubmit={this.updateUser}>

          <Alert show={error === true} type="danger">
            <strong>Error</strong> <span>{errorMessage}</span>
          </Alert>

          <Alert show={error === false} type="success">
            <strong>Success</strong> Your profile has been updated!
          </Alert>

          <div className="form-group">
            <label>Email:</label>
            <input type="text" className="form-control" value={email} disabled />
          </div>

          <div className="form-group">
            <label>Name:</label>
            <input type="text" className="form-control" value={name} disabled />
          </div>

          <div className="form-group">
            <label>Facebook Full Access Token:</label>
            <input
              type="text"
              className="form-control"
              value={value}
              onChange={this.handleChange}
              placeholder={
                !provider_full_access_token
                  ? 'Empty, paste your full access token from Facebook Profile page' :
                  'Updated, you can change other token here'
              }
            />
            <small className="form-text text-muted">Paste your full access token from Facebook Profile page</small>
          </div>

          <button type="submit" className="btn btn-primary">Update</button>

        </form>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
  user: state.user,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
    user: bindActionCreators(userActions, dispatch),
  },
});

export const Profile = withRouter(connect(mapStateToProps, mapActionToProps)(_Profile));
