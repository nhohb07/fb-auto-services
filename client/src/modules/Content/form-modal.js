import React, { Component } from 'react';
import { get } from 'lodash';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions } from 'reducer/actions';

import './styles.css';

import Content from 'services/Content';

export class _FormModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      _id: null,
      name: '',
      content: '',
      errors: {},
      errorMessage: null,
      isLocalUpdated: false,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (state.isLocalUpdated) {
      return { isLocalUpdated: false };
    }

    return {
      name: get(props, 'data.name', ''),
      content: get(props, 'data.content', ''),
      _id: get(props, 'data._id', ''),
      errors: {},
      errorMessage: null,
    };
  }

  /**
   * Change text
   */
  _handleChangeText = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
      errors: {},
      errorMessage: null,
      isLocalUpdated: true,
    });
  }

  /**
   * Submit form
   */
  _handleSubmit = async (event) => {
    event.preventDefault();

    const { name, content, _id } = this.state;
    const errors = {};

    if (!name) {
      errors.name = true;
    }

    if (!content) {
      errors.content = true;
    }

    if (Object.keys(errors).length) {
      return this.setState({
        errors,
        isLocalUpdated: true,
      });
    }

    const { spinner } = this.props.actions;
    spinner.show();

    let result;
    if (_id) {
      result = await Content.update({ _id, name, content });
    } else {
      result = await Content.create({ name, content });
    }

    spinner.hide();

    if (!result.success) {
      return this.setState({
        errorMessage: result.error,
        isLocalUpdated: true,
      });
    }

    this.props.onModalClosed(true);
  }

  /**
   * render()
   */
  render() {
    const { name, content, errors, _id } = this.state;
    const { onModalClosed } = this.props;

    return (
      <div
        ref={this.props.modalRef}
        className="modal fade"
        id="contentFormModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="contentFormModalLabel"
        aria-hidden="true"
        data-backdrop="static"
        show="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="contentFormModalLabel">
                {_id ? 'Edit content' : 'Add new content'}
              </h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={onModalClosed}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form onSubmit={this._handleSubmit}>
              <div className="modal-body">

                <div className="form-group">
                  <label htmlFor="ctrlName">Name</label>

                  <input
                    type="text"
                    className={['form-control', errors.name ? 'is-invalid' : ''].join(' ')}
                    id="ctrlName"
                    value={name}
                    onChange={this._handleChangeText}
                    name="name"
                  />

                  <div className="invalid-feedback">Please enter name for content</div>
                </div>

                <div className="form-group">
                  <label htmlFor="ctrlContent">Content</label>

                  <textarea
                    className={['form-control', errors.content ? 'is-invalid' : ''].join(' ')}
                    id="ctrlContent"
                    rows="3"
                    value={content}
                    onChange={this._handleChangeText}
                    name="content"
                  ></textarea>

                  <div className="invalid-feedback">Please enter content</div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={onModalClosed}>Close</button>
                <button type="submit" className="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
  },
});

export const FormModal = connect(mapStateToProps, mapActionToProps)(_FormModal);
