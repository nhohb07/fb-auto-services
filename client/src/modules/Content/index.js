/* eslint-disable camelcase */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions } from 'reducer/actions';

import { Layout } from 'modules';
import { FormModal } from './form-modal';

import './styles.css';

import Content from 'services/Content';

const $ = window.$;

export class _Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      selectedItem: null,
    };
  }

  /**
   * componentDidMount()
   */
  componentDidMount() {
    this._loadContents();
  }

  _loadContents = async () => {
    const { spinner } = this.props.actions;
    spinner.show();
    const { success, items } = await Content.query();
    spinner.hide();

    if (success) {
      this.setState({ items });
    }
  }

  _openModal = (selectedItem = null) => {
    this.setState({ selectedItem }, () => {
      $(this.modal).modal('show');
    });
  }

  async _handleDelete(item) {
    if (window.confirm('Are you sure to delete this item?')) {
      const { spinner } = this.props.actions;
      spinner.show();
      const { success, error } = await Content.delete(item._id);
      spinner.hide();

      if (success) {
        this.setState({ items: this.state.items.filter((i) => i._id !== item._id) });
      } else {
        alert(error);
      }
    }
  }

  _handleModalClosed = (reloadList = false) => {
    if (reloadList === true) {
      $(this.modal).modal('hide');
      this._loadContents();
    }
  }

  /**
   * render()
   */
  render() {
    const { items, selectedItem } = this.state;

    return (
      <Layout>
        <div className="text-right">
          <button
            type="button"
            className="btn btn-primary"
            onClick={this._openModal}
          >
            <i className="ion-android-add"></i> Add new
          </button>
        </div>
        <br />

        {
          items.length ? (
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col" width="1">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Content</th>
                  <th scope="col" width="150" className="text-right">Action</th>
                </tr>
              </thead>
              <tbody>
                {
                  items.map((item, index) => (
                    <tr key={index.toString()}>
                      <th scope="row">{index + 1}</th>
                      <td>{item.name}</td>
                      <td>{item.content}</td>
                      <td className="text-right">
                        <button
                          type="button"
                          className="btn btn-outline-secondary btn-sm"
                          data-toggle="modal"
                          data-target="#contentFormModal"
                          onClick={() => this._openModal(item)}
                        >
                          <i className="ion-edit"></i>
                        </button>

                        <button
                          type="button"
                          className="btn btn-outline-danger btn-sm"
                          onClick={() => this._handleDelete(item)}
                        >
                          <i className="ion-android-delete"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          ) : <div className="text-center text-muted">No record found!</div>
        }

        <FormModal
          modalRef={(el) => this.modal = el}
          data={selectedItem}
          onModalClosed={this._handleModalClosed}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
  user: state.user,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
  },
});

export const ContentComponent = withRouter(connect(mapStateToProps, mapActionToProps)(_Content));
