import React, { Component } from 'react';

import { Header } from 'components';

export class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div>
        <Header />

        <div className="container">
          {this.props.children}
        </div>
      </div>
    );
  }
}
