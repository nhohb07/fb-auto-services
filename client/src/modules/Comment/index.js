/* eslint-disable camelcase */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions } from 'reducer/actions';

import { Layout } from 'modules';
import { FormModal } from './form-modal';

import './styles.css';

import Comment from 'services/Comment';
import Content from 'services/Content';
import FacebookGroup from 'services/FacebookGroup';

const $ = window.$;

export class _Comment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contents: [],
      fb_groups: [],
      items: [],
      selectedItem: null,
    };
  }

  /**
   * componentDidMount()
   */
  componentDidMount() {
    this._loadData();
  }

  _loadData = async () => {
    const { spinner } = this.props.actions;
    spinner.show();

    // Load all Comments
    const comments = await Comment.query();
    if (!comments.success) {
      return alert(comments.error);
    }

    // Load all Contents
    const contents = await Content.query();
    if (!contents.success) {
      return alert(contents.error);
    }

    // Load all FB Groups
    const groups = await FacebookGroup.getGroups();
    if (!groups.success) {
      return alert(groups.error);
    }

    spinner.hide();

    this.setState({
      items: comments.items,
      contents: contents.items,
      fb_groups: groups.items,
    });
  }

  _openModal = (selectedItem = null) => {
    this.setState({ selectedItem }, () => {
      $(this.modal).modal('show');
    });
  }

  async _handleDelete(item) {
    if (window.confirm('Are you sure to delete this item?')) {
      const { spinner } = this.props.actions;
      spinner.show();
      const { success, error } = await Comment.delete(item._id);
      spinner.hide();

      if (success) {
        this.setState({ items: this.state.items.filter((i) => i._id !== item._id) });
      } else {
        alert(error);
      }
    }
  }

  _handleModalClosed = (reloadList = false) => {
    if (reloadList === true) {
      $(this.modal).modal('hide');
      this._loadData();
    }
  }

  /**
   * render()
   */
  render() {
    const { contents, fb_groups, items, selectedItem } = this.state;

    return (
      <Layout>
        <div className="text-right">
          <button
            type="button"
            className="btn btn-primary"
            onClick={this._openModal}
          >
            <i className="ion-android-add"></i> Add new
          </button>
        </div>
        <br />

        {
          items.length ? (
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col" width="1">#</th>
                  <th scope="col">Group Name</th>
                  <th scope="col">Contents</th>
                  <th scope="col">Interval</th>
                  <th scope="col">Next time</th>
                  <th scope="col" width="150" className="text-right">Action</th>
                </tr>
              </thead>
              <tbody>
                {
                  items.map((item, index) => (
                    <tr key={index.toString()}>
                      <th scope="row">{index + 1}</th>
                      <td>{item.fb_group_name}</td>
                      <td>
                        <ul className="content-list">
                          {item.contents.map((item, index) => (<li key={index.toString()}>{item.name}</li>))}
                        </ul>
                      </td>
                      <td>{item.interval} minutes</td>
                      <td>{item.next_time}</td>
                      <td className="text-right">
                        <button
                          type="button"
                          className="btn btn-outline-secondary btn-sm"
                          data-toggle="modal"
                          data-target="#contentFormModal"
                          onClick={() => this._openModal(item)}
                        >
                          <i className="ion-edit"></i>
                        </button>

                        <button
                          type="button"
                          className="btn btn-outline-danger btn-sm"
                          onClick={() => this._handleDelete(item)}
                        >
                          <i className="ion-android-delete"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          ) : <div className="text-center text-muted">No record found!</div>
        }

        <FormModal
          modalRef={(el) => this.modal = el}
          data={selectedItem}
          contents={contents}
          fb_groups={fb_groups}
          onModalClosed={this._handleModalClosed}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
  user: state.user,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
  },
});

export const CommentComponent = withRouter(connect(mapStateToProps, mapActionToProps)(_Comment));
