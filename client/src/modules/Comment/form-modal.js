import React, { Component } from 'react';
import { get } from 'lodash';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions } from 'reducer/actions';

import './styles.css';

import Comment from 'services/Comment';

export class _FormModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      errorMessage: null,
      isLocalUpdated: false,

      _id: null,
      selectedGroups: [],
      selectedContents: [],
      random_content: true,
      interval: '',
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (state.isLocalUpdated) {
      return { isLocalUpdated: false };
    }

    return {
      // name: get(props, 'data.name', ''),
      // content: get(props, 'data.content', ''),
      // _id: get(props, 'data._id', ''),
      // errors: {},
      // errorMessage: null,
    };
  }

  /**
   * Handle check/uncheck one item
   */
  _handleChecked = (event, type) => {
    const selected = (type === 'fb_groups') ? this.state.selectedGroups : this.state.selectedContents;

    const target = event.target;
    if (target.checked) {
      selected.push(target.value);
    } else {
      selected.splice(selected.indexOf(target.value), 1);
    }

    this.setState({
      [(type === 'fb_groups') ? 'selectedGroups' : 'selectedContents']: selected,
      errors: {},
    });
  }

  /**
   * Handle check all items
   */
  _handleCheckAll = (event, type) => {
    let selected = [];

    if (event.target.checked) {
      if (type === 'fb_groups') {
        selected = this.props.fb_groups.map((item) => item.id);
      } else {
        selected = this.props.contents.map((item) => item._id);
      }
    }

    this.setState({
      [(type === 'fb_groups') ? 'selectedGroups' : 'selectedContents']: selected,
      errors: {},
    });
  }

  /**
   * Change value
   */
  _handleChangeValue = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
      errors: {},
      errorMessage: null,
      isLocalUpdated: true,
    });
  }

  /**
 * Submit form
 */
  _handleSubmit = async (event) => {
    event.preventDefault();

    const { _id, selectedContents, selectedGroups, interval, random_content } = this.state;
    const errors = {};

    if (!selectedContents.length) {
      errors.selectedContents = true;
    }

    if (!selectedGroups.length) {
      errors.selectedGroups = true;
    }

    if (!interval) {
      errors.interval = true;
    }

    if (Object.keys(errors).length) {
      return this.setState({
        errors,
        isLocalUpdated: true,
      });
    }

    const contents = this.props.contents.filter((item) => selectedContents.indexOf(item._id) > -1);
    const groups = this.props.fb_groups.filter((item) => selectedGroups.indexOf(item.id) > -1);

    const { spinner } = this.props.actions;
    spinner.show();

    let result;
    if (_id) {
      result = await Comment.update({ _id, contents, groups, interval, random_content });
    } else {
      result = await Comment.create({ contents, groups, interval, random_content });
    }

    spinner.hide();

    if (!result.success) {
      return this.setState({
        errorMessage: result.error,
        isLocalUpdated: true,
      });
    }

    this.props.onModalClosed(true);
  }

  /**
   * render()
   */
  render() {
    const { errors, _id, selectedGroups, selectedContents } = this.state;
    const { onModalClosed, fb_groups, contents } = this.props;

    return (
      <div
        ref={this.props.modalRef}
        className="modal fade"
        id="contentFormModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="contentFormModalLabel"
        aria-hidden="true"
        data-backdrop="static"
        show="true"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="contentFormModalLabel">
                {_id ? 'Edit comment' : 'Add new comment'}
              </h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={onModalClosed}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form onSubmit={this._handleSubmit}>
              <div className="modal-body">

                <div className="form-group">
                  <fieldset className="fieldset-border">
                    <legend>Select fb groups</legend>

                    <div className="form-check form-check-all">
                      <input
                        className={['form-check-input', errors.selectedGroups ? 'is-invalid' : ''].join(' ')}
                        type="checkbox"
                        id="fb-group-select-all"
                        onChange={(event) => this._handleCheckAll(event, 'fb_groups')}
                      />
                      <label className="form-check-label" htmlFor="fb-group-select-all">Select all</label>
                      <div className="invalid-feedback">Please select as least a fb group.</div>
                    </div>

                    <div className="group-list">
                      {
                        fb_groups.map((group) => (
                          <div className="form-check" key={group.id}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value={group.id}
                              id={group.id}
                              checked={selectedGroups.indexOf(group.id) > -1}
                              onChange={(event) => this._handleChecked(event, 'fb_groups')}
                            />
                            <label className="form-check-label" htmlFor={group.id}>
                              {group.name}
                            </label>
                          </div>
                        ))
                      }
                    </div>
                  </fieldset>

                  <div className="invalid-feedback">Please select as least a fb group</div>
                </div>

                <div className="form-group">
                  <fieldset className="fieldset-border">
                    <legend>Select content</legend>

                    <div className="row">
                      <div className="col">
                        <div className="form-check form-check-all">
                          <input
                            className={['form-check-input', errors.selectedContents ? 'is-invalid' : ''].join(' ')}
                            type="checkbox"
                            id="contents-select-all"
                            onChange={(event) => this._handleCheckAll(event, 'contents')}
                          />
                          <label className="form-check-label" htmlFor="contents-select-all">Select all</label>
                          <div className="invalid-feedback">Please select as least a fb group.</div>
                        </div>
                      </div>

                      <div className="col">
                        <div className="form-check form-check-all">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="random-contents"
                            checked={this.state.random_content}
                            name="random_content"
                            onChange={this._handleChangeValue}
                          />
                          <label className="form-check-label" htmlFor="random-contents">Random contents</label>
                        </div>
                      </div>
                    </div>

                    <div className="group-list">
                      {
                        contents.map((content) => (
                          <div className="form-check" key={content._id}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value={content._id}
                              id={content._id}
                              checked={selectedContents.indexOf(content._id) > -1}
                              onChange={(event) => this._handleChecked(event, 'contents')}
                            />
                            <label className="form-check-label" htmlFor={content._id}>
                              {content.name}
                            </label>
                          </div>
                        ))
                      }
                    </div>
                  </fieldset>

                  <div className="invalid-feedback">Please select as least a content</div>
                </div>

                <div className="form-group">
                  <label htmlFor="interval">Interval</label>

                  <select
                    id="interval"
                    className={['form-control', errors.interval ? 'is-invalid' : ''].join(' ')}
                    defaultValue={this.state.interval}
                    name="interval"
                    onChange={this._handleChangeValue}
                  >
                    <option>Choose...</option>
                    <option value="1">1 minute</option>
                    <option value="30">30 minutes</option>
                    <option value="60">1 hour</option>
                    <option value="90">1,5 hours</option>
                    <option value="120">2 hours</option>
                    <option value="150">2,5 hours</option>
                    <option value="180">3 hours</option>
                  </select>

                  <div className="invalid-feedback">Please select a time</div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={onModalClosed}>Close</button>
                <button type="submit" className="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
  },
});

export const FormModal = connect(mapStateToProps, mapActionToProps)(_FormModal);
