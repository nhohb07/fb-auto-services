import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { withRouter } from 'react-router-dom';

import './styles.css';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions, userActions } from 'reducer/actions';

import User from 'services/User';

class _Login extends Component {
  async componentDidMount() {
    await User.logout();
    this.props.actions.user.clear();
  }

  responseFacebook = async (credential) => {
    if (!credential.accessToken) {
      return;
    }

    const { spinner } = this.props.actions;
    spinner.show();

    try {
      const user = await User.login(credential);

      if (user.success) {
        User.saveLoginData(user.data);
        this.props.actions.user.set(user.data);
        this.props.history.replace('/');
      }
    } catch (error) { }

    spinner.hide();
  }

  render() {
    return (
      <div className="form-center">
        <FacebookLogin
          appId="704358149902576"
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
          render={(renderProps) => (
            <button onClick={renderProps.onClick} type="button" className="btn btn-success">
              Login with your Facebook
            </button>
          )}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
    user: bindActionCreators(userActions, dispatch),
  },
});

export const Login = withRouter(connect(mapStateToProps, mapActionToProps)(_Login));
