import API from './API';

class Comment {
  query() {
    return API.get('comments');
  }

  create(data) {
    return API.post('comments', data);
  }

  update(data) {
    return API.put('comments', data);
  }

  delete(id) {
    return API.delete('comments', { id });
  }
}

export default new Comment();
