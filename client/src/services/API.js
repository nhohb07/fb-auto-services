const messages = require('messages');

class API {
  async request(method, endpoint, data = {}, options = {}) {
    const API_ENDPOINT = `${process.env.REACT_APP_API_ENDPOINT}/${endpoint}`;

    const headers = {
      'Content-Type': 'application/json',
    };

    if (options.useToken !== false) {
      const userToken = localStorage.getItem('userToken');
      headers.Authorization = `Bearer ${userToken}`;
    }

    const request = {
      method,
      headers,
    };

    if (['GET', 'HEAD'].indexOf(method) > -1) {
      request.params = JSON.stringify(data);
    } else {
      request.body = JSON.stringify(data);
    }

    try {
      const response = await fetch(API_ENDPOINT, request);

      if (response.status !== 200) {
        throw new Error(messages[response.status]);
      }

      // Get json data
      const responseJson = await response.json();

      if (!responseJson.success) {
        throw new Error(responseJson.error);
      }

      return responseJson;
    } catch (error) {
      console.log('API', error);

      return {
        success: false,
        error: error.message,
      };
    }
  }

  async get(endpoint, data = {}, options = {}) {
    return await this.request('GET', endpoint, data, options);
  }

  async post(endpoint, data = {}, options = {}) {
    return await this.request('POST', endpoint, data, options);
  }

  async put(endpoint, data = {}, options = {}) {
    return await this.request('PUT', endpoint, data, options);
  }

  async delete(endpoint, data = {}, options = {}) {
    return await this.request('DELETE', endpoint, data, options);
  }

  async head(endpoint, data = {}, options = {}) {
    return await this.request('HEAD', endpoint, data, options);
  }
}

export default new API();
