import API from './API';

class User {
  async checkLogin() {
    const userToken = localStorage.getItem('userToken');

    if (!userToken) {
      return false;
    }

    // Get User Profile
    const profile = await API.get('user');

    if (!profile.success) {
      this.logout();

      return false;
    }

    this.profile = profile.data;

    return true;
  }

  async login(credential) {
    return await API.post('user', credential, { useToken: false });
  }

  saveLoginData(data) {
    for (let key in data) {
      if (['name', 'provider_id', 'userToken'].indexOf(key) > -1) {
        localStorage.setItem(key, data[key]);
      }
    }
  }

  getProfile() {
    return this.profile;
  }

  async logout() {
    return localStorage.clear();
  }

  update(data) {
    const id = localStorage.getItem('provider_id');
    return API.put('user', { ...data, id });
  }
}

export default new User();
