import API from './API';

class FacebookComment {
  create(data) {
    return API.post('fb-comments', data);
  }
}

export default new FacebookComment();
