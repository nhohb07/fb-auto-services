import API from './API';

class FacebookGroup {
  getGroups() {
    return API.get('fb-groups');
  }
}

export default new FacebookGroup();
