import API from './API';

class Content {
  query() {
    return API.get('contents');
  }

  create(data) {
    return API.post('contents', data);
  }

  update(data) {
    return API.put('contents', data);
  }

  delete(id) {
    return API.delete('contents', { id });
  }
}

export default new Content();
