import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import './styles/App.css';

import { Spinner } from './components';
import {
  Home,
  Login,
  Profile,
  ContentComponent,
  CommentComponent,
} from './modules';

import User from 'services/User';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { spinnerActions, userActions } from 'reducer/actions';

class App extends Component {
  async componentDidMount() {
    const { spinner } = this.props.actions;
    spinner.show();
    const isLogged = await User.checkLogin();
    spinner.hide();

    if (this.props.location.pathname !== '/login' && !isLogged) {
      return this.props.history.replace('/login');
    }

    this.props.actions.user.set(User.getProfile());
  }

  render() {
    return (
      <div className="full-size">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/profile" component={Profile} />
          <Route path="/contents" component={ContentComponent} />
          <Route path="/comments" component={CommentComponent} />
        </Switch>
        <Spinner />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
});

const mapActionToProps = (dispatch) => ({
  actions: {
    spinner: bindActionCreators(spinnerActions, dispatch),
    user: bindActionCreators(userActions, dispatch),
  },
});

export default withRouter(connect(mapStateToProps, mapActionToProps)(App));
