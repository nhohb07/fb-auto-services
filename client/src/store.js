import { createStore, combineReducers } from 'redux';

import { spinner, user } from 'reducer';

const appReducer = combineReducers({
  spinner,
  user,
});

export default createStore(appReducer);
