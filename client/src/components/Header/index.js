import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';
// Redux
import { connect } from 'react-redux';

import './styles.css';

import User from 'services/User';

class _Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  logout = () => {
    User.logout();
    this.props.history.replace('/login');
  }

  render() {
    const { name, provider_id } = this.props.user;
    const avatar = (!provider_id) ? 'images/avatar.svg' : `http://graph.facebook.com/${provider_id}/picture?type=square`;

    const currentPath = _.get(this.props.history, 'location.pathname', '/');

    return (
      <header>
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <div className="container">
            <Link className="navbar-brand" to="/">NTV 2018</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarCollapse">
              <div className="mr-auto">
                <ul className="navbar-nav">
                  <li className={['nav-item', currentPath === '/' ? 'active' : ''].join(' ')}>
                    <Link className="nav-link" to="/">Home</Link>
                  </li>
                  <li className={['nav-item', currentPath === '/contents' ? 'active' : ''].join(' ')}>
                    <Link className="nav-link" to="/contents">Content</Link>
                  </li>
                  <li className={['nav-item', currentPath === '/comments' ? 'active' : ''].join(' ')}>
                    <Link className="nav-link" to="/comments">Comments</Link>
                  </li>
                </ul>
              </div>

              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src={avatar} alt="avatar" className="profile-avatar" />
                    <span>{name}</span>
                  </a>

                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <Link className="dropdown-item" to="/profile">Profile</Link>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" onClick={this.logout}>Logout</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export const Header = withRouter(connect(mapStateToProps)(_Header));
