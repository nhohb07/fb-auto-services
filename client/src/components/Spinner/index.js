import React, { Component } from 'react';

// Redux
import { connect } from 'react-redux';

import './styles.css';

class _Spinner extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    if (!this.props.spinner.isShow) {
      return null;
    }

    return (
      <div className="spinner">
        <div className="spinner-icon"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  spinner: state.spinner,
});

export const Spinner = connect(mapStateToProps)(_Spinner);
