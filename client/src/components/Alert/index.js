import React, { Component } from 'react';

export class Alert extends Component {
  render() {
    const { show = false, type = 'success' } = this.props;

    if (!show) {
      return null;
    }

    return (
      <div className={`alert alert-${type}`} role="alert">
        {this.props.children}

        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    );
  }
};
