export const userActions = {
  set(userInfo) {
    return {
      userInfo,
      type: 'SET_USER_INFO',
    };
  },

  clear() {
    return { type: 'CLEAR_USER_INFO' };
  },
};
