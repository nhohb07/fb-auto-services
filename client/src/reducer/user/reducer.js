/* eslint-disable indent */
const initialState = {
  userInfo: {},
};

export const user = (state = initialState, { type, userInfo }) => {
  switch (type) {
    case 'SET_USER_INFO':
      return {
        ...state,
        ...userInfo,
      };

    case 'CLEAR_USER_INFO':
      return initialState;

    default:
      return state;
  }
};
