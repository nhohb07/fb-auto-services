/* eslint-disable indent */
const initialState = {
  isShow: false,
};

export const spinner = (state = initialState, { type }) => {
  switch (type) {
    case 'SHOW_SPINNER':
      return {
        ...state,
        isShow: true,
      };

    case 'HIDE_SPINNER':
      return {
        ...state,
        isShow: false,
      };

    default:
      return state;
  }
};
