export const spinnerActions = {
  show() {
    return {
      isShow: true,
      type: 'SHOW_SPINNER',
    };
  },

  hide() {
    return {
      isShow: false,
      type: 'HIDE_SPINNER',
    };
  },
};
